TAP
===

TAP aims to provide tools to easily create and deliver mobile tour applications in a museum setting. Content creation is performed in the content management system, Drupal. TAP tours are exportable into an intermediate format, TourML, which can then be used as pluggable bundles for mobile applications.

License
-------
TAP is released under the GPLv3 license.  See GPL.txt for more information

Installation
------------
